https://github.com/react-icons/react-icons

# Description
This is a reactjs ⚛️ project building with classes and redux. This is an application to get information about the weather ☁️ in different countries around the world 🌎

# Made with
[![ReactJS](https://img.shields.io/badge/react-61dafb?style=for-the-badge&logo=react&logoColor=white&labelColor=000000)]()
[![JavaScript](https://img.shields.io/badge/javascript-ead547?style=for-the-badge&logo=javascript&logoColor=white&labelColor=000000)]()